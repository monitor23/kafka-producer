package com.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MTEventsProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MTEventsProducerApplication.class, args);
	}

}
