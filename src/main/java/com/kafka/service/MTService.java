package com.kafka.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class MTService {

    private final char CR = (char) 0x0D;

    public List<String> readContent(MultipartFile uploadedFile) throws IOException {
        List<String> mtContent = new ArrayList<>();
        InputStream stream = uploadedFile.getInputStream();
        InputStreamReader isReader = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isReader);
        String line;
        String output = "";
        int i=0;
        while (((line = reader.readLine()) != null)) {
            if (line.equals("-}")) {
                output=output+ CR+"\n-}";
                mtContent.add(output);
                output = new String();
                i=0;
            } else {
                if (i == 0) {
                    output = String.valueOf(output) + line;
                    i++;
                } else if (i == 1) {
                    output = String.valueOf(output) + CR + "\n" + line;
                }
            }

 }
        return mtContent;
    }
}
