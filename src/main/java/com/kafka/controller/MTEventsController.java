package com.kafka.controller;

import com.kafka.dto.ResponseDTO;
import com.kafka.service.KafkaEventProducer;
import com.kafka.service.MTService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
public class MTEventsController {
    private MTService mtService;
    private KafkaEventProducer kafkaEventProducer;

    public MTEventsController(MTService mtService, KafkaEventProducer kafkaEventProducer) {
        this.mtService = mtService;
        this.kafkaEventProducer = kafkaEventProducer;
    }

    @PostMapping(value = "/batchUpload", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        List<String> mtList = mtService.readContent(file);
        for(String test: mtList) {
            log.info(test);
        }
        kafkaEventProducer.sendMtEvent(mtList);
        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }


}
